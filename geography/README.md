# Geography <!-- omit in toc -->

- [Polygons](#polygons)
  - [Bidding zones](#bidding-zones)
  - [Countries](#countries)
  - [Metadata](#metadata)
  - [License and copyright information](#license-and-copyright-information)
  - [References](#references)
- [Postal codes](#postal-codes)
  - [License](#license)

## Polygons

### Bidding zones

`data/geography/polygons/bidding_zones.geojson`

**Last download date: 10.07.2020**

This file contains polygons representing the following bidding zones, which are derived from nomenclature of territorial units for statistics (NUTS) 2016 level 0 regions:

- Switzerland (CH)
- Czech Republic (CZ)
- Germany, Austria, and Luxembourg (DE-AT-LU)
- the Netherlands (NL)
- Poland (PL)

For France (FR), NUTS 2016 level 1 regions were obtained and merged in order to exclude France's overseas territories from the bidding zone polygon.

It also contains polygons representing the Eastern Denmark (DK-2) and Western Denmark (DK-1) bidding zones, which are derived from NUTS 2016 level 2 regions.

Polygons approximately representing bidding zones in Sweden are from [Tomorrow's electricityMap](https://github.com/tmrowco/electricitymap-contrib)'s [third-party maps](https://github.com/tmrowco/electricitymap-contrib/tree/master/web/third_party_maps). These polygons were originally derived from [Natomraden.se](https://www.natomraden.se/).

It is created by running the Python script `scripts/zones.py`. The script parses the following datasets:

- from [Eurostat](https://gisco-services.ec.europa.eu/distribution/v2/nuts/nuts-2016-files.html):
  - NUTS_RG_01M_2016_4326_LEVL_0.geojson
  - NUTS_RG_01M_2016_4326_LEVL_1.geojson
  - NUTS_RG_01M_2016_4326_LEVL_2.geojson
- from [Tomorrow's electricityMap](https://github.com/tmrowco/electricitymap-contrib/tree/master/web/third_party_maps):
  - SE-SE1.json
  - SE-SE2.json
  - SE-SE3.json
  - SE-SE4.json

### Countries

`data/geography/polygons/countries.geojson`

**Last download date: 05.07.2020**

This file contains polygons for the following countries based on NUTS 2016 regions:

- Austria
- Switzerland
- Czech Republic
- Germany
- Denmark
- France - excluding overseas territories
- Luxembourg
- the Netherlands
- Poland
- Sweden

It is created by running the Python script `scripts/countries.py`. The script parses the following dataset from [Eurostat](https://gisco-services.ec.europa.eu/distribution/v2/nuts/nuts-2016-files.html):

- NUTS_RG_01M_2016_4326_LEVL_0.geojson
- NUTS_RG_01M_2016_4326_LEVL_1.geojson

### Metadata

Complete metadata is in the [metadata.pdf](https://gisco-services.ec.europa.eu/distribution/v2/nuts/nuts-2016-metadata.pdf) file. See also the [Eurostat website](https://ec.europa.eu/eurostat/web/nuts/background).

**File naming**

pattern: theme_spatilatype_resolution_year_projection_subset.format

example: NUTS_RG_01M_2016_4326_LEVL_0.geojson

theme: 4-character code of theme (NUTS)

spatialtype: RG: regions (multipolygons)

resolution: 01M; map scale the data is optimized (generalised) for.

year: the year of NUTS regulation (2016). See <https://ec.europa.eu/eurostat/web/nuts/history>.

projection: 4-digit EPSG code, see <https://spatialreference.org/ref/epsg/>.

- EPSG:4326 (WGS84, coordinates in decimal degrees)

subset: one of NUTS levels

- LEVL_0: NUTS level 0 (countries)
- LEVL_1: NUTS level 1
- LEVL_2: NUTS level 2
- LEVL_3: NUTS level 3

Each subset makes complete coverage (RG data).
No subset code - all NUTS levels are in the same file. It means quadruple coverage (RG data); any point hits 4 (or 0) polygons.

format: `.geojson` (<https://geojson.org>)

### License and copyright information

#### NUTS 2016

© European Union, 1995 - today

Eurostat has a policy of encouraging free re-use of its data, both for non-commercial and commercial purposes. All statistical data, metadata, content of web pages or other dissemination tools, official publications and other documents published on its website, with the exceptions listed below, can be reused without any payment or written licence provided that:

- the source is indicated as Eurostat;
- when re-use involves modifications to the data or text, this must be stated clearly to the end user of the information.

In addition to the general copyright and licence policy applicable to the whole Eurostat website, the following specific provisions apply to the Administrative units / Statistical units datasets downloaded. The download and usage of these data is subject to the acceptance of the following clauses:

- The Commission agrees to grant the non-exclusive and not transferable right to use and process the Eurostat/GISCO geographical data downloaded from this page (the "data").
- The permission to use the data is granted on condition that: (1) the data will not be used for commercial purposes; (2) the source will be acknowledged. A copyright notice, as specified below, will have to be visible on any printed or electronic publication using the data downloaded from this page.
- Data source will have to be acknowledged in the legend of the map and in the introductory page of the publication with the following copyright notice: **© EuroGeographics for the administrative boundaries**.

#### electricityMap

electricityMap by Tomorrow is licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).

### References

- [NUTS 2016 release notes](https://gisco-services.ec.europa.eu/distribution/v2/nuts/nuts-2016-release-notes.txt)
- [GISCO data distribution API](https://gisco-services.ec.europa.eu/distribution/v2/nuts/)
- [Eurostat copyright notice and free re-use of data](https://ec.europa.eu/eurostat/about/policies/copyright)
- [Administrative units / Statistical units download provisions](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units)

## Postal codes

`data/geography/postcodes/postcodesDE.csv` is a CSV file containing German postcodes, which is created by running the Python script `scripts/postcodes.py`. This script downloads data from the [GeoNames Postal Code dataset](https://www.geonames.org/postal-codes/).

### License

**The following descriptions are derived from the readme file obtained during download.**

This readme describes the GeoNames Postal Code dataset.
The main GeoNames gazetteer data extract is here: <https://download.geonames.org/export/dump/>.

This work is licensed under a Creative Commons Attribution 3.0 License.
This means you can use the dump as long as you give credit to geonames (a link on your website to www.geonames.org is ok).
See <https://creativecommons.org/licenses/by/3.0/>.

For many countries, latitudes and longitudes are determined with an algorithm that searches the place names in the main geonames database
using administrative divisions and numerical vicinity of the postal codes as factors in the disambiguation of place names.

For postal codes and place name for which no corresponding toponym in the main geonames database could be found an average
latitudes and longitudes of 'neighbouring' postal codes is calculated.

Please let GeoNames know if you find any errors in the data set.

The data format is tab-delimited text in UTF-8 encoding, with the following fields:

- postal_code: varchar(20)
- place_name: varchar(180)
- admin_name1: 1. order subdivision (state) varchar(100)
- admin_code1: 1. order subdivision (state) varchar(20)
- admin_name2: 2. order subdivision (county/province) varchar(100)
- admin_code2: 2. order subdivision (county/province) varchar(20)
- admin_name3: 3. order subdivision (community) varchar(100)
- admin_code3: 3. order subdivision (community) varchar(20)
- latitude: estimated latitude (wgs84)
- longitude: estimated longitude (wgs84)
- accuracy: accuracy of latitudes and longitudes from 1=estimated, 4=geonameid, 6=centroid of addresses or shape
