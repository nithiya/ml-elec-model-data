# Power system <!-- omit in toc -->

- [Renewable power generators](#renewable-power-generators)
  - [Translations](#translations)
- [ENTSO-E Transparency Platform data](#entso-e-transparency-platform-data)
  - [Installed generation capacity](#installed-generation-capacity)
  - [Generation](#generation)
  - [Prices](#prices)
  - [Load](#load)

## Renewable power generators

**Last download date: 02.07.2020**

The script `scripts/rpp.py` is used to extract and translate the following data.

The following information is obtained from this site and roughly translated into English: <https://www.netztransparenz.de/EEG/Anlagenstammdaten>.

Netztransparenz - Erneuerbare-Energien-Gesetz (EEG, which roughly translates to Renewable Energy Sources Act) plant data: EEG system master data for the 2018 annual accounts.

The system master data reported to the transmission system operator by the network operators, who are obliged to purchase and pay for it, as part of the 2018 EEG annual accounts, are available for download as a file.

The file contains the master data of all EEG systems that were reported by the DSOs to the TSOs as part of the 2018 EEG annual report. This can lead to decommissioning during the year as well as to network transitions between two network operators, which results in double mentioning of the system key. In general, the master data reflects the status as of December 31, 2018 or the day of decommissioning or network exit.

The files also include the systems connected directly or indirectly to the transmission system operators' networks. For data protection reasons, the street and house number are not given for systems with an installed output of less than 30 kW.

Datasets are available for each transmission system operator (TSO):

- 50Hertz Transmission
- Amprion
- TenneT TSO
- TransnetBW

These were extracted, translated, combined, and saved into the following datasets, grouped by energy carrier:

- Onshore wind and offshore wind: `data/power/installed/wind.csv`

Based on these, and the postal code and meteorological datasets, the following files were created, which includes the approximate geographical location of power plants aggregated by postal code and their closest meteorological station:

- Onshore wind and offshore wind: `data/power/installed/wind_agg.csv`

The script `scripts/rpp_agg.py` is used to create the data above.

### Translations

Since the official data is in German, some translations are made.

Column names:

- EEG_Anlagenschluessel: EEG_plant_key
- NB_BNR: NB_BNR
- Netzbetreiber: network_operator
- Straße_Flurstueck: address
- PLZ: postal_code
- Ort_Gemarkung: city_district
- Gemeindeschluessel: municipality_key
- Bundesland: state
- Installierte_Leistung: installed_capacity (kW)
- Energietraeger: energy_carrier
- Einspeisespannungsebene: feed_in_voltage_level
- Leistungsmessung: power_measurement
- Regelbarkeit: controllability
- Inbetriebnahme: commissioning
- Ausserbetriebnahme: decommissioning
- Netzzugang: network_connection
- Netzabgang: network_disconnection

Energy carriers:

- Wasser: hydro
- Biomasse: biomass
- Wind an Land: onshore wind
- Deponiegas: landfill gas
- Wind auf See: offshore wind
- Klärgas: sewage gas
- Geothermie: geothermal
- Grubengas: mine gas

Power measurement:

- Nein: no
- Ja: yes

State:

- Ausschließliche Wirtschaftszone: exclusive economic zone
- Ausland: foreign country

Controllability:

- nicht regelbar: not adjustable
- 70 % Begrenzung: 70 % limit
- regelbar n. § 9 Abs. 2 EEG: adjustable according to § 9 Abs. 2 EEG
- regelbar n. § 9 Abs. 1 EEG: adjustable according to § 9 Abs. 1 EEG

Feed-in voltage level (TBD):

- NS
- MS
- MS/NS
- HöS
- HS
- HS/MS
- HöS/HS

## ENTSO-E Transparency Platform data

The script `scripts/entsoe_data.py` is used to extract the following data from the European Network of Transmission Systems Operators for Electricity Transparency Platform (`ENTSO-E TP).

The [ENTSO-E TP](https://transparency.entsoe.eu/) has a dashboard with various electricity system data tables and visualisations available to the public. The **list of data available for free re-use** is [available on the website](https://transparency.entsoe.eu/content/static_content/download?path=/Static%20content/terms%20and%20conditions/191025_List_of_Data_available_for_reuse_v2_cln.pdf). All users must first accept the platform's [terms and conditions and privacy policy](https://transparency.entsoe.eu/content/static_content/Static%20content/terms%20and%20conditions/terms%20and%20conditions.html) before gaining access to the dashboard. However, in order to export datasets in various formats (such as CSV and XML), as well as gain additional functionalities, it is required to [register for a free account](https://transparency.entsoe.eu/usrm/user/createPublicUser) on the ENTSO-E TP. ENTSO-E TP's Restful application programming interface (API) can then be used to automate the data extraction process (see the [API implementation](https://transparency.entsoe.eu/content/static_content/download?path=/Static%20content/web%20api/RestfulAPI_IG.pdf) and [user guides](https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html) for more info). Once a free account has been created, request for a security token to access the API by sending an email to the ENTSO-E TP Helpdesk (transparency at entsoe dot eu), stating 'Restful API access' in the subject and the email address used to register for the account. Once granted, the security token can be viewed via account settings.

The [ENTSO-E API Python client (entsoe-py)](https://github.com/EnergieID/entsoe-py) is used to easily query the required data and return them as pandas dataframes or series. The queries for generation and installed generation capacity per unit return dataframes, while the query for load returns a series.

The bidding zones in Germany and its interconnections, mapped to their corresponding [Energy Identification Codes](https://www.entsoe.eu/data/energy-identification-codes-eic/) (EICs) are used when querying using the pandas client.

**Note:** Note that `DE-LU` only works for timestamps starting 01.10.2018. Use `DE-AT-LU` for timestamps prior to this date. ([More info.](https://transparency.entsoe.eu/news/widget?id=5b7c1e9b5092e75a10bab903)) Since this project is focussing on the first half of 2018, `DE-AT-LU` is used.

### Installed generation capacity

**Last download date: 10.07.2020**

Installed generation capacity grouped by generation technology in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `installed_generation_capacity_DE-AT-LU.csv`

### Generation

**Last download date: 10.07.2020**

- Time series of 15-minute electricity generation grouped by generation technology in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/generation/generation_DE-AT-LU.csv`
- Time series of day-ahead hourly electricity generation forecast in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/generation/generation_forecast_DE-AT-LU.csv`
- Time series of day-ahead 15-minute electricity generation forecast grouped by generation technology (solar, offshore wind, onshore wind) in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/generation/wind_and_solar_forecast_DE-AT-LU.csv`

### Prices

**Last download date: 10.07.2020**

Time series of day-ahead hourly electricity market prices in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/prices/day_ahead_prices_DE-AT-LU.csv`

### Load

**Last download date: 10.07.2020**

- Time series of 15-minute electricity load in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/load/load_DE-AT-LU.csv`
- Time series of 15-minute electricity load forecast in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018: `data/power/load/load_forecast_DE-AT-LU.csv`
