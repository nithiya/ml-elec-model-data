# Meteorology <!-- omit in toc -->

Data downloaded from the Deutscher Wetterdienst (DWD; German meteorological service) [Climate Data Center](https://opendata.dwd.de/climate_environment/CDC/) (CDC).

**The period of data used is 01.01.2018 - 01.07.2018**

- [List of stations](#list-of-stations)
  - [Translations](#translations)
- [Wind](#wind)
- [License and copyright](#license-and-copyright)

## List of stations

**Last download date: 01.09.2020**

The file named `data/meteorology/stations.csv` contains the list of meteorological stations in Germany for each observation type obtained by running the Python script `scripts/met_stations.py`.

### Translations

Since the official data is in German, some translations are made.

Column names for lists of meteorological stations:

- Stations_id: station_id
- von_datum: start_date
- bis_datum: end_date
- Stationshoehe: station_height
- geoBreite: latitude
- geoLaenge: longitude
- Stationsname: station_name
- Bundesland: state

## Wind

**Last download date: 01.09.2020**

Files in the `data/meteorology/wind` directory contains historical hourly station observations of wind speed and wind direction for Germany. These files are obtained by running the Python script `scripts/met_data.py`.

See <https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/wind/historical/DESCRIPTION_obsgermany_climate_hourly_wind_historical_en.pdf> for the full description. Accessed version (version v006, 2018) was last edited on 19.12.2018.

The file comprises following parameters:

- STATIONS_ID: station identification number
- MESS_DATUM_ENDE: end of measurement interval (yyyymmddhh)
- QN_3: quality level of next columns; coding see paragraph "Quality information"
- F: mean wind speed (m/s)
- D: mean wind direction (Grad)
- eor: end of record; can be ignored

with missing values are marked as -999. The definition of measurement time changed over time and referred to time units MOZ, MEZ or UTC (see the station specific Metadaten_Parameter* for the exact definition). Nowadays, hourly wind speed and wind direction is given as the average of the six 10 min intervals measured in the previous hour (e.g., at UTC 11, the average wind speed and average wind direction during UTC 10 - UTC 11 is given).

## License and copyright

**Terms of use for data on the CDC ftp server**

**Please note:** This [English translation](https://opendata.dwd.de/climate_environment/CDC/Terms_of_use.pdf) is intended as a convenience to non-German-reading customers and has no legal effect for compliance or enforcement purposes. Only the [German version](https://opendata.dwd.de/climate_environment/CDC/Nutzungsbedingungen_German.pdf) shall be legally binding.

**All data in the freely accessible area of the CDC's ftp server are protected by copyright.**

The freely accessible data may be re-used without any restrictions provided that the source reference is indicated, as laid down in the GeoNutzV ordinance ("Verordnung zur Festlegung der Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes = Ordinance to Determine the Conditions for Use for the Provision of Spatial Data of the Federation). As to the layout of source references, the DWD requests adherence to the following guidelines (cf. § 7 of the DWD Law and § 3 of the GeoNutzV ordinance):

- The obligation to indicate the enclosed source references shall apply to any spatial data and other services of the DWD that are used without alteration. Source references must also be indicated even if extracts or excerpts are used or if the data format has been changed. Displaying the DWD logo shall be considered as meeting the requirement of source reference in meaning of the GeoNutzV ordinance.
- In the event of more advanced alteration, processing, new design or other adaptation, DWD at least expects to be mentioned in a central list of references or in an imprint.
- Indication of alteration according to the GeoNutzV ordinance may read as follows: "Data basis: Deutscher Wetterdienst, gridded data reproduced graphically"; "Data basis: Deutscher Wetterdienst, averaged over individual values", or "Data basis: Deutscher Wetterdienst, own elements added".
- If a service provided by the DWD is used in a way that does not comply with its intended purpose, the enclosed source references have to be deleted. This shall especially apply to weather warnings for which there is no guarantee that they are delivered to all users at all times completely and without delay.

You will find corresponding explanations and templates for the implementation under: <https://www.dwd.de/EN/service/copyright/templates_dwd_as_source.html>.

Wherever data or information from third parties is used for the generation of DWD-own products and services, the Deutscher Wetterdienst assures that it holds all necessary rights to do so.

Source of geospatial base data: Surveying authorities of the Länder and Federal Agency for Cartography and Geodesy (<https://www.bkg.bund.de>).

Source of satellite data: EUMETSAT (<https://www.eumetsat.int>), NOAA (<https://www.noaa.gov>).

**Please ensure the copyright conditions stated in the data set descriptions are met.**
